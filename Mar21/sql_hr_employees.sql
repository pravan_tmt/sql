-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sql_hr
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (33391,'D\'arcy','Nortunen','Account Executive',62871,37270,1,435235235),(37270,'Yovonnda','Magrannell','Executive Secretary',63996,NULL,10,NULL),(37851,'Sayer','Matterson','Statistician III',98926,37270,1,435235235),(40448,'Mindy','Crissil','Staff Scientist',94860,37270,1,435235235),(56274,'Keriann','Alloisi','VP Marketing',110150,37270,1,435235235),(63196,'Alaster','Scutchin','Assistant Professor',32179,37270,2,435235235),(67009,'North','de Clerc','VP Product Management',114257,37270,2,435235235),(67370,'Elladine','Rising','Social Worker',96767,37270,2,435235235),(68249,'Nisse','Voysey','Financial Advisor',52832,37270,2,435235235),(72540,'Guthrey','Iacopetti','Office Assistant I',117690,37270,3,435235235),(72913,'Kass','Hefferan','Computer Systems Analyst IV',96401,37270,3,435235235),(75900,'Virge','Goodrum','Information Systems Manager',54578,37270,3,435235235),(76196,'Mirilla','Janowski','Cost Accountant',119241,37270,3,435235235),(80529,'Lynde','Aronson','Junior Executive',77182,37270,4,435235235),(80679,'Mildrid','Sokale','Geologist II',67987,37270,4,435235235),(84791,'Hazel','Tarbert','General Manager',93760,37270,4,435235235),(95213,'Cole','Kesterton','Pharmacist',86119,37270,4,435235235),(96513,'Theresa','Binney','Food Chemist',47354,37270,5,435235235),(98374,'Estrellita','Daleman','Staff Accountant IV',70187,37270,5,435235235),(115357,'Ivy','Fearey','Structural Engineer',92710,37270,5,435235235);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-21 11:51:23
